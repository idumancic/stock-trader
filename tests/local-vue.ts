import "@/plugins/vee-validate";
import BootstrapVue from "bootstrap-vue";
import { ValidationProvider, ValidationObserver } from "vee-validate";
import { createLocalVue } from "@vue/test-utils";

const localVue = createLocalVue();
localVue.use(BootstrapVue);
localVue.component("validation-provider", ValidationProvider);
localVue.component("validation-observer", ValidationObserver);
localVue.filter("currency", (value: string | number) => {
  return `$${value.toLocaleString()}`;
});

export default localVue;

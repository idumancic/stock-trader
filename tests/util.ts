import { Wrapper } from "@vue/test-utils";
import flushPromises from "flush-promises";

export function vm<T extends Vue | null>(wrapper: Wrapper<T>): any {
  return wrapper.vm as any;
}

export function payload<T extends Vue | null>(
  wrapper: Wrapper<T>,
  eventName: string
): any[][] {
  return wrapper.emitted()[eventName] as any[][];
}

export async function flush(): Promise<void> {
  await flushPromises();
  jest.runAllTimers();
}

import { Wrapper, mount } from "@vue/test-utils";
import { payload, vm } from "../util";

import { BButton } from "bootstrap-vue";
import BaseStock from "@/components/BaseStock.vue";
import { OrderStock } from "@/types";
import { Stock } from "@/types";
import localVue from "../local-vue";

const stock: Stock = { id: 1, name: "test", price: 10 };

describe("@/components/BaseStock.vue", () => {
  let wrapper: Wrapper<BaseStock>;

  beforeEach(() => {
    wrapper = mount(BaseStock, {
      localVue,
      propsData: { stock }
    });
  });

  it("is a Vue instance", () => {
    expect(wrapper.vm).toBeTruthy();
  });

  it("renders valid component", () => {
    expect(wrapper.find("h5").text()).toContain("test");
    expect(wrapper.find(".badge").text()).toContain(10);
    expect(vm(wrapper).quantity).toBe(0);
  });

  it("disables action button when quantity is zero", async () => {
    const button = wrapper.findComponent(BButton);
    await wrapper.setData({ quantity: 0 });

    expect(button.text()).toContain("Action");
    expect(button.attributes()?.disabled).toBe("disabled");
  });

  it("emits action event on action button click and resets quantity", async () => {
    const button = wrapper.findComponent(BButton);
    await wrapper.setData({ quantity: 2 });

    expect(button.element.innerHTML).toContain("Action");
    expect(vm(wrapper).quantity).toBe(2);

    await button.trigger("click");

    expect(vm(wrapper).quantity).toBe(0);
    expect(wrapper.emitted().action).toBeTruthy();
    expect(payload(wrapper, "action")[0][0]).toMatchObject<OrderStock>({
      quantity: 2,
      stockId: stock.id,
      stockPrice: stock.price
    });
  });
});

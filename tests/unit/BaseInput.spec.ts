import { BFormInput, BFormInvalidFeedback } from "bootstrap-vue";
import { Wrapper, mount } from "@vue/test-utils";
import { payload, flush } from "../util";

import BaseInput from "@/components/BaseInput.vue";
import localVue from "../local-vue";

jest.useFakeTimers();

describe("@/components/BaseInput.vue", () => {
  let wrapper: Wrapper<BaseInput>;

  beforeEach(() => {
    wrapper = mount(BaseInput, {
      localVue,
      attrs: {
        id: "test-input",
        name: "test-input",
        type: "text"
      },
      propsData: {
        label: "Test Label"
      },
      data: () => ({
        modelValue: null
      })
    });
  });

  it("is a Vue instance", () => {
    expect(wrapper.vm).toBeTruthy();
  });

  it("renders valid component", () => {
    const formInput = wrapper.findComponent(BFormInput);
    const formLabel = wrapper.find("label");

    expect(formInput.attributes().id).toBe("test-input");
    expect(formInput.attributes().name).toBe("test-input");
    expect(formInput.attributes().type).toBe("text");

    expect(formLabel.text()).toBe("Test Label");
    expect(formLabel.attributes().for).toBe("test-input");
  });

  it("renders asterisk on label if required rule is active", async () => {
    await wrapper.setProps({ rules: "required" });
    const formLabel = wrapper.find("label");
    expect(formLabel.text()).toContain("*");
  });

  it("emits input event on model value change", async () => {
    const formInput = wrapper.findComponent(BFormInput);
    await formInput.setValue("test");

    expect(wrapper.emitted().input).toBeTruthy();
    expect(payload(wrapper, "input")[0]).toEqual(["test"]);
  });

  it("shows valid state on valid user input value", async () => {
    wrapper.setProps({ rules: "required" });
    const formInvalidFeedback = wrapper.findComponent(BFormInvalidFeedback);
    const formInput = wrapper.findComponent(BFormInput);
    formInput.setValue("test");

    await flush();

    expect(formInput.classes()).toContain("is-valid");
    expect(formInvalidFeedback.text()).not.toBeTruthy();
  });

  it("shows invalid state on invalid user input value", async () => {
    wrapper.setProps({ rules: "required" });
    const formInvalidFeedback = wrapper.findComponent(BFormInvalidFeedback);
    const formInput = wrapper.findComponent(BFormInput);
    formInput.setValue(null);

    await flush();

    expect(formInput.classes()).toContain("is-invalid");
    expect(formInvalidFeedback.text()).toBeTruthy();
  });
});

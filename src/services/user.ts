import { HttpService } from "@/http-service";
import { HttpInterceptor } from "@/decorators/http-interceptor";
import { AuthResponse, AuthFormData } from "@/types";

@HttpInterceptor
class UserService extends HttpService {
  constructor() {
    super({
      baseURL: "https://identitytoolkit.googleapis.com/v1/"
    });
  }

  login(data: AuthFormData) {
    return this.instance.post<AuthResponse>(
      `accounts:signInWithPassword?key=${process.env.VUE_APP_API_KEY}`,
      {
        email: data.email,
        password: data.password,
        returnSecureToken: true
      }
    );
  }

  signup(data: AuthFormData) {
    return this.instance.post<AuthResponse>(
      `accounts:signUp?key=${process.env.VUE_APP_API_KEY}`,
      {
        email: data.email,
        password: data.password,
        returnSecureToken: true
      }
    );
  }
}

export default new UserService();

import { HttpService } from "@/http-service";
import { HttpInterceptor } from "@/decorators/http-interceptor";
import { StockPortfolioData, StockTraderResponse } from "@/types";

@HttpInterceptor
class StockTraderService extends HttpService {
  saveData(userId: string, data: StockPortfolioData) {
    return this.instance.post(`${userId}.json`, data, {
      cancelToken: this.cancelTokenSource.token
    });
  }

  loadData(userId: string) {
    return this.instance.get<StockTraderResponse>(`${userId}.json`, {
      cancelToken: this.cancelTokenSource.token
    });
  }
}

export default new StockTraderService();

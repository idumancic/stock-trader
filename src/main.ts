import "@babel/polyfill";
import "mutationobserver-shim";
import "reflect-metadata";
import Vue from "vue";
import { BvToast } from "bootstrap-vue";

import "./plugins/bootstrap-vue";
import "./plugins/vee-validate";

import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;

Vue.config.errorHandler = (err: Error, vm: Vue & { $bvToast: BvToast }) => {
  console.log(err.stack);
  vm.$bvToast.toast(err.message, {
    title: "Stock Trader",
    variant: "danger"
  });
};

Vue.filter("currency", (value: string | number) => {
  return `$${value.toLocaleString()}`;
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

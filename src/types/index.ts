export interface Stock {
  id: number;
  name: string;
  price: number;
}

export interface OrderStock {
  stockId: number;
  stockPrice: number;
  quantity: number;
}

export interface StockPortfolio {
  id: number;
  name?: string;
  price?: number;
  quantity: number;
}

export interface StockPortfolioData {
  funds: number;
  stockPortfolio: StockPortfolio[];
  stocks: Stock[];
}

export interface StockTraderResponse {
  [key: string]: StockPortfolioData;
}

export interface AuthFormData {
  email: string | null;
  password: string | null;
}

export interface AuthResponse {
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
}

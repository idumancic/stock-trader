/* eslint-disable @typescript-eslint/no-use-before-define */
import { getStoreBuilder } from "vuex-typex";
import { RootState } from "@/store";
import { AuthFormData } from "@/types";
import userService from "@/services/user";

export interface UserState {
  idToken: string | null;
  userId: string | null;
}

const initialState: UserState = {
  idToken: null,
  userId: null
};

const module = getStoreBuilder<RootState>().module("user", initialState);

const getters = {
  authToken: module.read(state => state.idToken, "authToken"),
  userId: module.read(state => state.userId, "userId")
};

const mutations = {
  setAuthCredentials(
    state: UserState,
    payload: { idToken: string | null; userId: string | null }
  ) {
    state.idToken = payload.idToken;
    state.userId = payload.userId;
  }
};

const actions = {
  async signup(_: any, authData: AuthFormData) {
    const data = await userService.signup(authData);
    user.commitSetAuthCredentials({
      idToken: data.idToken,
      userId: data.localId
    });
  },
  async login(_: any, authData: AuthFormData) {
    const data = await userService.login(authData);
    user.commitSetAuthCredentials({
      idToken: data.idToken,
      userId: data.localId
    });
  },
  logout() {
    user.commitSetAuthCredentials({ idToken: null, userId: null });
  }
};

const user = {
  get state() {
    return module.state();
  },

  get authToken() {
    return getters.authToken();
  },

  get userId() {
    return getters.userId();
  },

  commitSetAuthCredentials: module.commit(mutations.setAuthCredentials),

  dispatchLogin: module.dispatch(actions.login),
  dispatchLogout: module.dispatch(actions.logout),
  dispatchSignup: module.dispatch(actions.signup)
};

export default user;

import "reflect-metadata";
import { PropOptions } from "vue";
import { Constructor } from "vue/types/options";
import { createDecorator } from "vue-class-component";

function applyMetadata(options: any, target: object, key: string | symbol) {
  if (
    !Array.isArray(options) &&
    typeof options !== "function" &&
    typeof options.type === "undefined"
  ) {
    const type = Reflect.getMetadata("design:type", target, key);
    if (type !== Object) {
      options.type = type;
    }
  }
}

function capitalize(text: string) {
  return text.charAt(0).toUpperCase() + text.slice(1);
}

export function Model(
  event: string,
  options: PropOptions | Constructor[] | Constructor = {}
) {
  return (target: Vue, propertyName: string) => {
    applyMetadata(options, target, propertyName);
    createDecorator((componentOptions, key) => {
      componentOptions.model = { prop: propertyName, event };

      componentOptions.props = {
        ...(componentOptions.props || ({} as any)),
        [propertyName]: options
      };

      componentOptions.computed = {
        ...(componentOptions.computed || ({} as any)),
        [`vModel${capitalize(key)}`]: {
          get() {
            return (this as any)[propertyName];
          },
          set(value) {
            (this as Vue).$emit(event, value);
          }
        }
      };
    })(target, propertyName);
  };
}

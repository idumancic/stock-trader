import axios, { AxiosResponse, AxiosError, AxiosRequestConfig } from "axios";
import { HttpService } from "@/http-service";
import { global, user } from "@modules";

declare module "axios" {
  /*
    override of AxiosResponse interface because of response destructuring
    to get data object because TS would throw error otherwise
  */
  // eslint-disable-next-line
  interface AxiosResponse<T> extends Promise<T> {}
}

export const HttpInterceptor = function<
  T extends { new (...args: any[]): HttpService }
>(originalConstructor: T) {
  return class extends originalConstructor {
    private requestCounter = 0;

    constructor(...args: any[]) {
      super(...args);
      this.useRequestInterceptor();
      this.useResponseInterceptor();
    }

    private useRequestInterceptor() {
      this.instance.interceptors.request.use(
        this.handleRequest.bind(this),
        this.handleError.bind(this)
      );
    }

    private useResponseInterceptor() {
      this.instance.interceptors.response.use(
        this.handleResponse.bind(this),
        this.handleError.bind(this)
      );
    }

    private handleRequest(config: AxiosRequestConfig) {
      if (user.authToken) {
        config.params = {
          ...config.params,
          auth: user.authToken
        };
      }

      this.requestCounter++;
      global.commitSetIsLoading(true);
      return config;
    }

    private handleResponse({ data }: AxiosResponse) {
      if (--this.requestCounter === 0) {
        global.commitSetIsLoading(false);
      }

      return data;
    }

    private handleError(error: AxiosError) {
      if (--this.requestCounter === 0) {
        global.commitSetIsLoading(false);
      }

      if (axios.isCancel(error)) {
        return;
      }

      return Promise.reject(error);
    }
  };
};

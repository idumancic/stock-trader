import { extend } from "vee-validate";
import { required, confirmed, email, min } from "vee-validate/dist/rules";

extend("required", {
  ...required,
  message: "This field is required"
});

extend("email", {
  ...email,
  message: "This field must be a valid email"
});

extend("confirmed", {
  ...confirmed,
  message: "This field confirmation does not match"
});

extend("min", {
  ...min,
  message: (_, params) => {
    return `This field should be at least ${params.length} characters`;
  }
});
